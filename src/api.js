import * as FS from './utilities/fs.js';
import Issuer from './vc/issuer.js';
import Verify from './vc/verifier.js';

// Third Party Modules
import EXPRESS from 'express';

var ROUTER = EXPRESS.Router();

let cIssuer = null;
let sCurAgency = null;
let sCurCredential = null;
let aCurAddContexts = null

// Get all the credentials (file system search)
ROUTER.get('/credentials', async (req, res, next) => {

    let aCredentials = null

    try {
        
        aCredentials = await FS.GLOB('./src/vc/credentials/**/*.credential.hbs');
    }
    catch (err) {
        console.log("Error looking for credentials!");
    }

    let objResponce = {};

    if (aCredentials && aCredentials.length) {

        // Loop through all of 
        for (let c = 0, cLen = aCredentials.length; c < cLen; c++) {

            let asFilePath = aCredentials[c].split('/');

            let sFilename = asFilePath[asFilePath.length - 1];
            let sAgency = asFilePath[asFilePath.length - 3];

            if (!objResponce[sAgency]) {
                objResponce[sAgency] = [];
            }

            objResponce[sAgency].push(sFilename.replace('.credential.hbs', ''));

        }

        res.json(objResponce);
    }
    else {

        res.status('401');
        //res.render('error', { error: "test" });
        res.send("Error!");
    }

});

// Get the meta data to the requested credential (comes in agencyName-credentialName formate)
ROUTER.post('/credentialMeta', async (req, res, next) => {

    const body = req.body;

    if (body.credential) {

        let aParts = body.credential.split('-');

        let sAgency = aParts[0].toLowerCase();
        let sCredential = aParts[1].toLowerCase();

        sCurAgency = sAgency;
        sCurCredential = sCredential;

        // Look up
        // -- Config
        // -- Template

        let aCredFiles = null;

        // Build the request string
        let sGlobString = `./src/vc/credentials/${sAgency}/${sCredential}/${sCredential}.**.*`;

        try {
        
            aCredFiles = await FS.GLOB(sGlobString);
        }
        catch (err) {

            console.log(`Error looking for credential files for ${sAgency}-${sCredential}!`);
        }

        if (aCredFiles && aCredFiles.length) {

            // Clear the issuer class as a new one has been selected
            cIssuer = null;
            sCurAgency = null;
            sCurCredential = null;
            aCurAddContexts = null;

            let sCredentialTemplate = null;
            let oCredentialConfig = null;

            // Loop through all of the files
            for (let sFileName of aCredFiles) {

                let sFileContents = null;

                try {

                    sFileContents = await FS.READ_FILE(sFileName);
                }
                catch (err) {

                    throw new Error(`Error occured when attempting to read file: ${sFileName}\n${err}`);
                }

                if (sFileName.indexOf('config') !== -1) {
                    oCredentialConfig = JSON.parse(sFileContents);
                }
                else {
                    sCredentialTemplate = sFileContents;
                }

            }

            // Create a new credential issuer instance
            cIssuer = new Issuer(sCredentialTemplate, sAgency, sCredential, oCredentialConfig.contexts);
            sCurAgency = sAgency;
            sCurCredential = sCredential;
            aCurAddContexts = oCredentialConfig.contexts;

            res.json(oCredentialConfig);
        }

    }

});

// Gets all the credential data from the page form
ROUTER.post(`/credentialCreate`, async (req, res, next) => {

    const oBody = req.body;

    let asKeyPaths = null;

    // Build the public/private key path
    let sKeyPath = `./src/vc/keys/${sCurAgency}/${sCurCredential}/**.jwk`;

    try {
        
        asKeyPaths = await FS.GLOB(sKeyPath);
    }
    catch (err) {

        console.log(`Error looking for credential files for ${sCurAgency}-${sCurCredential}!`);
    }

    await cIssuer.setup(asKeyPaths);

    let oCredential = await cIssuer.issue(oBody);

    res.json(oCredential);

});

ROUTER.post(`/credentialVerify`, async (req, res, next) => {

    const oBody = req.body;

    let sCredential = oBody.credential;

    let cVerify = new Verify(sCredential, aCurAddContexts);

    await cVerify.setup();

    let bResult = await cVerify.verify()

    res.json({result: bResult});

});


export default ROUTER;