// First Party
import { READ_FILE } from '../utilities/fs.js';

// Third Party Modules
import jsonLdDocumentLoader from 'jsonld-document-loader';
import { verifiable } from "@transmute/vc.js";
import jsonWebSig from "@transmute/json-web-signature";

const JsonWebSignature = jsonWebSig.JsonWebSignature;

// Standard Contexts
import W3C_CRED_V1 from './contexts/w3c-cred-v1.js';
import W3C_EXP_CRED_V1 from './contexts/w3c-cred-example-v1.js';
import W3C_JSON_WEB_SIG_V1 from './contexts/w3c-json-web-sig-2020-v1.js';
import ODRL_CONTEXT from './contexts/odrl-context.js';
import DID_CONTEXT from './contexts/did-context.js';
import NYS_BASE from './contexts/nys-credential.js';

import AXIOS from 'axios';

export default class Verify {

    constructor(sCredential, aAddContexts) {

        this.oCredential = JSON.parse(sCredential, aAddContexts);
        this.aAddContexts = aAddContexts;

        // Load the normal documents
        this.documentLoader = new jsonLdDocumentLoader.JsonLdDocumentLoader();

        this.documentLoader.addStatic('https://www.w3.org/2018/credentials/v1', W3C_CRED_V1);
        this.documentLoader.addStatic('https://w3c-ccg.github.io/lds-jws2020/contexts/v1', W3C_JSON_WEB_SIG_V1);
        this.documentLoader.addStatic('https://www.w3.org/2018/credentials/examples/v1', W3C_EXP_CRED_V1);
        this.documentLoader.addStatic('https://www.w3.org/ns/odrl.jsonld', ODRL_CONTEXT);
        this.documentLoader.addStatic('https://www.w3.org/ns/did/v1', DID_CONTEXT);
        this.documentLoader.addStatic('https://credential.ny.gov/base.jsonld', NYS_BASE);

        this.documentContext = null;
    }

    async setup() {

        // Check for additional contexts
        if (this.aAddContexts) {

            for (let oContext of this.aAddContexts) {

                let oContextJSONLD = JSON.parse(await READ_FILE(`./src/vc/contextsDynamic/${oContext.context}`));

                this.documentLoader.addStatic(oContext.url, oContextJSONLD);
            }

        }

        this.documentContext = this.documentLoader.build();

    }

    verify() {

        return new Promise((res, rej) => {

            let sCredentialDid = this.oCredential.issuer;

            let DOC_CONTEXT = this.documentContext;
    
            AXIOS.get(sCredentialDid)
                .then(async (resp) => {
    
                    let oDidFile = resp.data;
    
                    const result2 = await verifiable.credential.verify({
            
                        credential: this.oCredential,
                        format: ['vc'],
                        documentLoader: async (iri) => {
              
                            console.log(`iri: ${iri}`);
              
                            if (iri.startsWith("https://localhost:4000") || iri == "credential.json" || iri === this.oCredential.issuer || iri === this.oCredential.proof.verificationMethod) {
    
                                return {
                                    documentUrl: iri,
                                    document: oDidFile,
                                };
                            }
    
                            return DOC_CONTEXT(iri);
              
                        },
                        suite: [new JsonWebSignature()],
                    });

                    if (result2.verified) {

                        res(true);
                    }
                    else {

                        res(false);
                    }
    
                })
                .catch((err) => {
    

                    throw new Error (`Error occured when attempting to get credential did:\n${err}`);
                })
        });

    }

}