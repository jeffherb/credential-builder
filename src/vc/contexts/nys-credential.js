// Web ORDL Context
// URL Path: https://credential.ny.gov/base.jsonld
export default {
    "@context": {
        "id": "@id",
        "type": "@type",
        "xsd": "http://www.w3.org/2001/XMLSchema#",
        "guid": {"@id": "xsd:string", "@type": "@id"},
    }
  }