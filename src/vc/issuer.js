// Native Modules
import CRYPTO from 'crypto';

// First Party
import { READ_FILE } from '../utilities/fs.js';

// Third Party Modules
import * as JOSE from 'jose';
import jsonLdDocumentLoader from 'jsonld-document-loader';
import { verifiable } from "@transmute/vc.js";
import Handlebars from 'handlebars';
import jsonWebSig from "@transmute/json-web-signature";

// Standard Contexts
import W3C_CRED_V1 from './contexts/w3c-cred-v1.js';
import W3C_EXP_CRED_V1 from './contexts/w3c-cred-example-v1.js';
import W3C_JSON_WEB_SIG_V1 from './contexts/w3c-json-web-sig-2020-v1.js';
import ODRL_CONTEXT from './contexts/odrl-context.js';
import NYS_BASE from './contexts/nys-credential.js';

export default class Issuer {

    constructor(sCredentialTemplate, sAgencyCode, sCredentialType, aAddContexts) {

        this.sAgencyCode = sAgencyCode;
        this.sCredentialType = sCredentialType;

        this.cHandlebars = Handlebars

        this.cTemplate = this.cHandlebars.compile(sCredentialTemplate);

        this.oCredential = null;

        this.oPrivateJwk = null;
        this.oPublicJwk = null;

        this.JWK = null;
        this.suite = null;

        this.JsonWebKey = jsonWebSig.JsonWebKey;
        this.JsonWebSignature = jsonWebSig.JsonWebSignature;

        this.aAddContexts = aAddContexts || null;

        // Load the normal documents
        this.documentLoader = new jsonLdDocumentLoader.JsonLdDocumentLoader();

        this.documentLoader.addStatic('https://www.w3.org/2018/credentials/v1', W3C_CRED_V1);
        this.documentLoader.addStatic('https://w3c-ccg.github.io/lds-jws2020/contexts/v1', W3C_JSON_WEB_SIG_V1);
        this.documentLoader.addStatic('https://www.w3.org/2018/credentials/examples/v1', W3C_EXP_CRED_V1);
        this.documentLoader.addStatic('https://www.w3.org/ns/odrl.jsonld', ODRL_CONTEXT);
        this.documentLoader.addStatic('https://credential.ny.gov/base.jsonld', NYS_BASE);

        this.documentContext = null;
    }

    async thumbPrint() {

        return await JOSE.calculateJwkThumbprint(this.oPrivateJwk);
    }

    async setup(asKeyPaths) {

        let sPrivateKeyPath = null;
        let sPublicKeyPath = null;

        for (let sKey of asKeyPaths) {

            if (sKey.toLowerCase().indexOf('private') !== -1) {
                sPrivateKeyPath = sKey;
            }
            else {
                sPublicKeyPath = sKey;
            }

        }

        try {

            this.oPrivateJwk = JSON.parse(await READ_FILE(sPrivateKeyPath));
        }
        catch(err) {

            throw new Error(`Error occured when attempting to read the private key:\n${err}`);
        }

        try {

            this.oPublicJwk = JSON.parse(await READ_FILE(sPublicKeyPath));
        }
        catch(err) {

            throw new Error(`Error occured when attempting to read the public key:\n${err}`);
        }

        // Check for additional contexts
        if (this.aAddContexts) {

            for (let oContext of this.aAddContexts) {

                let oContextJSONLD = JSON.parse(await READ_FILE(`./src/vc/contextsDynamic/${oContext.context}`));

                this.documentLoader.addStatic(oContext.url, oContextJSONLD);
            }

        }

        this.documentContext = this.documentLoader.build();
    }

    async issue(oData) {

        let sCredential = null;

        // Create a random GUID
        oData.guid = CRYPTO.randomBytes(16).toString("hex");
        oData.id = `https://credential.ny.gov/${this.sAgencyCode}/${this.sCredentialType}`;

        try {

            sCredential = this.cTemplate(oData);
        }
        catch(err) {

            console.log(`Error occured when merging the data with the handlebars template:\n${err}`);
        }

        // Parse the credential into an object
        this.oCredential = JSON.parse(sCredential);

        // Generate the thumbprint its needed for below
        this.sThumbPrint = await this.thumbPrint();
        
        // At this point we should have everything, lets build the key that will be used to sign the credential
        this.JWK = await this.JsonWebKey.from({
            id: this.oCredential.issuer + "#" + this.sThumbPrint,
            type: "JsonWebKey2020",
            controller:  this.oCredential.issuer.replace('credential', 'keys'),
            publicKeyJwk: this.oPublicJwk,
            privateKeyJwk: this.oPrivateJwk
        });

        this.suite = new this.JsonWebSignature({
            key: this.JWK
        });

        // Create place for the signed credential
        let oSignedCredential = null;

        try {
            
            oSignedCredential = await verifiable.credential.create({
                credential: this.oCredential,
                format: ["vc"],
                documentLoader: this.documentContext,
                suite: this.suite
            });
        }
        catch(err) {

            console.log(`Error while generating the credential:\n${err}`);

            return false;
        }

        // Get the credential from the items array
        oSignedCredential = oSignedCredential.items[0];

        return this.oCredential;
    }

}