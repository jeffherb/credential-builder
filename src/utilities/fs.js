
// Native modules
import FSP from 'fs/promises';
import PATH from 'path';

// Third Party
import { globby } from 'globby';

export async function GLOB() {
    
    return await globby(...arguments);;
}

// Read a file
export async function READ_FILE(sPath) {

    try {

        return await FSP.readFile(sPath, { encoding: "utf8"});
    }
    catch(err) {

        throw new Error(`Error reading file: ${sPath}:\n${err}`);
    }

}

// Write a file
export async function WRITE_FILE(sPath, sContents) {

    try {

        await FSP.writeFile(sPath, sContents, { encoding: "utf8"});
    }
    catch(err) {

        throw new Error(`Error writing file: ${sPath}:\n${err}`);

    }
    
}

// Recursively create directorys
export async function MKDIR(sPath) {

    try {

        await FSP.mkdir(sPath, {recursive: true});
    }
    catch (err) {

        throw new Error(`Error creating folder path: ${sPath}:\n${err}`);
    }
}