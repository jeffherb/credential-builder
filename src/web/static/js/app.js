
// Common fetch function
async function fAjaxFetch(sUrl) {

    let response, data = null;

    // Execute request
    response = await fetch(sUrl);

    if (!response.ok) {

        const message = `An error has occured: ${response.status}`;
        throw new Error(message);
    }
    else {

        data = response.json();
    }

    return data;

}

async function fSendData(sUrl, oHeader, oData) {

    let response, data = null;

    if (!oHeader) {
        oHeader = {
            "method": "POST",
            "headers": {
                "Content-type": "application/json"
            },
            "body": JSON.stringify(oData)
        }
    }

    response = await fetch(sUrl, oHeader);

    if (!response.ok) {

        const message = `An error has occured: ${response.status}`;
        throw new Error(message);
    }
    else {

        data = await response.json();
    }

    return data;
}

function fCapWord(sWord) {

    return sWord.slice(0,1).toUpperCase() + sWord.slice(1);
}

function fClearContents(vElem) {

    if (typeof vElem === "string") {
        vElem = document.querySelector(vElem);
    }

    if (vElem.nodeType) {

        while(vElem.firstChild){
            vElem.removeChild(vElem.firstChild);
        }

    }

}

export default class App {

    constructor() {

        // DOM Elements
        this.dCredentialDropdown = document.querySelector('#credentials');
        this.dCredentialInputs = document.querySelector('#credential-inputs');
        this.dPreCode = document.querySelector('#code-view');
        this.dQrCode = document.querySelector('#qr-view');
        this.dVerify = document.querySelector('#verifyControls');

        // Basic fields
        this.oStandardCredentialInputs = [
            {
                type: "date",
                label: "Issuance Date",
                name: "issuanceDate"
            },
            {
                type: "date",
                label: "Expiration Date",
                name: "expirationDate"
            }
        ]

    }

    // Get and update the credential dropdown
    async getCredentials() {

        let oCredentials = null;

        try {

            oCredentials = await fAjaxFetch('/api/credentials');
        }
        catch(err) {
            
            console.log(err);
        }

        if (Object.keys(oCredentials).length) {

            let dNewSelect = document.createElement('select');
            dNewSelect.setAttribute('id', 'credentials');

            let blankOption = document.createElement('option');
            blankOption.setAttribute("value", "");
            blankOption.appendChild(document.createTextNode( "Select Cred" ));

            dNewSelect.appendChild(blankOption);

            for (let sAgency in oCredentials) {

                let dNewOptionGroup = document.createElement('optgroup');
                dNewOptionGroup.setAttribute('label', sAgency.toUpperCase());

                for (let sCred of oCredentials[sAgency]) {

                    let dNewOption = document.createElement('option');
                    dNewOption.setAttribute('value', `${sAgency}-${sCred}`);
                    dNewOption.appendChild(document.createTextNode( fCapWord(sCred) ));

                    dNewOptionGroup.appendChild(dNewOption);
                }

                dNewSelect.appendChild(dNewOptionGroup);
            }

            let dParentNode = this.dCredentialDropdown.parentElement;

            dParentNode.removeChild(this.dCredentialDropdown);

            dParentNode.appendChild(dNewSelect);

            this.dCredentialDropdown = dNewSelect;
        }
        else {

            // Clear out the credentials?
        }

    }

    getCredentialMeta(oMeta) {

        fClearContents(this.dCredentialInputs);

        function fCreateFieldSet(sFieldSetLegend, aFieldSetContents, sNameSpace) {

            let dFieldSet = document.createElement('fieldset');
            dFieldSet.setAttribute('data-namespace', sNameSpace);
    
            let dFieldSetLegend = document.createElement('legend');
            dFieldSetLegend.appendChild(document.createTextNode(sFieldSetLegend));
    
            let dFieldSetContainer = document.createElement('div');

            // Append the legend and input container
            dFieldSet.appendChild(dFieldSetLegend);
            dFieldSet.appendChild(dFieldSetContainer);

            if (aFieldSetContents && aFieldSetContents.length) {

                for (let oField of aFieldSetContents) {

                    if (oField.type === "fieldset") {

                        let dSubFieldSet = fCreateFieldSet(oField.legend, oField.fields, (`${sNameSpace}.${oField.namespace}`));

                        dFieldSetContainer.appendChild(dSubFieldSet);
                    }
                    else {

                        let dField = fCreateInput(oField, sNameSpace);

                        dFieldSetContainer.appendChild(dField);
                    }

                }

            }

            return dFieldSet;
        }

        function fCreateInput(oField, sNameSpace) {

            let dRow = document.createElement('div');
            dRow.classList.add('row');

            let dLabel = null;
            let dInputControl = null;

            dLabel = document.createElement('label');
            dLabel.appendChild(document.createTextNode(oField.label));

            dRow.appendChild(dLabel);

            switch (oField.type) {

                case "text":

                    dInputControl = document.createElement("input");
                    dInputControl.setAttribute("type", "text");

                    break;

                case "date":

                    dInputControl = document.createElement("input");
                    dInputControl.setAttribute("type", "date");
                    break;

                case "select":

                    dInputControl = document.createElement('select');

                    let dBlankOption = document.createElement('option');
                    dBlankOption.setAttribute('value', "");
                    dBlankOption.appendChild(document.createTextNode("Select Value"));

                    dInputControl.appendChild(dBlankOption);

                    for (let oOption of oField.options) {

                        let dOption = document.createElement('option');
                        dOption.setAttribute('value', oOption.value);
                        dOption.appendChild(document.createTextNode(oOption.text));

                        dInputControl.appendChild(dOption);
                    }

                    break;

            }

            dInputControl.setAttribute('name', (`${sNameSpace}.${oField.name}`));

            dRow.appendChild(dInputControl);

            return dRow;
        }

        // Check For subject information
        if (oMeta.subject && oMeta.subject.fields) {

            let dFieldSet = fCreateFieldSet('Credential Subject:', oMeta.subject.fields, "cred.subject");

            this.dCredentialInputs.appendChild(dFieldSet);

        }

        // Create the basic fields
        let dStandardFields = fCreateFieldSet('Credential Properties:', this.oStandardCredentialInputs, 'cred');

        this.dCredentialInputs.appendChild(dStandardFields);

        let dButtonContainers = document.createElement('div')
        dButtonContainers.classList.add("user-input-buttons");

        let dGenerateError = document.createElement('span');
        dGenerateError.setAttribute('id', 'gen-error');
        dGenerateError.classList.add('error');

        dButtonContainers.appendChild(dGenerateError);

        let dSubmitButton = document.createElement('button');
        dSubmitButton.appendChild(document.createTextNode("Issue Credential"));
        dSubmitButton.setAttribute("type", "button");
        dSubmitButton.addEventListener('click', async () => {
            
            let oCredPayload = {
                issuanceDate: null,
                expirationDate: null,
                subject: {}
            };

            let adInputs = this.dCredentialInputs.querySelectorAll('input,select');

            if (adInputs && adInputs.length) {

                for (let dInput of adInputs) {

                    let sName = dInput.getAttribute('name').replace('cred.', '');

                    let asDataPath = sName.split('.');

                    let oTarget = oCredPayload;

                    // Loop throught the namespace
                    for (let d = 0, dLen = asDataPath.length; d < dLen; d++) {

                        if (d + 1 >= dLen) {

                            if (dInput.nodeName === "INPUT") {

                                switch (dInput.getAttribute('type')) {

                                    case "date":

                                        if (dInput.value) {

                                            let cDate = new Date(dInput.value);

                                            oTarget[asDataPath[d]] = cDate.toISOString().slice(0, 19) + 'Z';
                                        }

                                        break;

                                    default:
                                        oTarget[asDataPath[d]] = dInput.value;
                                        break;

                                }

                            }
                            else {

                                oTarget[asDataPath[d]] = dInput.value;
                            }
                            
                        }
                        else {

                            // Create the namespace in the credential payload
                            if (!oTarget[asDataPath[d]]) {
                                oTarget[asDataPath[d]] = {};
                            }

                            oTarget = oTarget[asDataPath[d]];
                        }

                    }

                    
                }
            
                let oCredential = await fSendData('./api/credentialCreate', false, oCredPayload);

                if (oCredential === false) {

                    let dGenerateError = document.querySelector('#gen-error');

                    dGenerateError.appendChild(document.createTextNode("Error occured when generating credential. Please see node console."));

                }
                else {

                    this.buildResults(oCredential);
                }


            }


        })

        dButtonContainers.appendChild(dSubmitButton);

        this.dCredentialInputs.appendChild(dButtonContainers);
    }

    buildQr(sCodeContents) {

        let oConfig = {
            height: 500,
            width: 500,
            correctLevel: QRCode.CorrectLevel.L,
            text: sCodeContents
        };

        try {

            let qr = new QRCode(this.dQrCode, oConfig);
    
            //qr.resize(oConfig.height, oConfig.width);
        }
        catch(err) {

            console.log("QR Code was unable to be generated. Most likely too complex to fit on the screen")

            let dQRError = document.createElement('span');
            dQRError.classList.add("error");
            dQRError.appendChild(document.createTextNode("QR-Code was too complex to display"));

            this.dQrCode.appendChild(dQRError);
        }

    }

    async buildResults(oCredential) {

        // Force clear incase we already have something
        fClearContents(this.dPreCode);
        fClearContents(this.dQrCode);
        fClearContents(this.dVerify);
        
        let sStringCredential = JSON.stringify(oCredential, null, 4);
        let sMinifiedCredential = JSON.stringify(oCredential);

        let dPre = document.createElement('pre');
        let dCode = document.createElement('code');
        dCode.setAttribute('class', "language-json");

        dCode.appendChild(document.createTextNode(sStringCredential));

        dPre.appendChild(dCode);

        this.dPreCode.appendChild(dPre);

        this.buildQr(sMinifiedCredential);

        let dVerifyRow = document.createElement('div');
        dVerifyRow.classList.add('row');

        let dVerifyButton = document.createElement('button');
        dVerifyButton.setAttribute('type', "button");
        dVerifyButton.appendChild(document.createTextNode('Verify Credential'));
        dVerifyButton.addEventListener('click', async () => {

            let oData = await fSendData('./api/credentialVerify', false, { credential: sMinifiedCredential });

            let dVerifyResult = dVerifyRow.querySelector('span');
            let bAdd = false;

            if (dVerifyResult === null) {
                dVerifyResult = document.createElement('span');
                bAdd = true;
            }
            else {
                dVerifyResult.classList.remove('error');
                dVerifyResult.classList.remove('success');
                fClearContents(dVerifyResult);
            }

            if (result) {

                dVerifyResult.classList.add('success');
                dVerifyResult.appendChild(document.createTextNode("Verified"));
            }
            else {

                dVerifyResult.classList.add('error');
                dVerifyResult.appendChild(document.createTextNode("Failed to validate! See node console."));
            }

            if (bAdd) {
                dVerifyRow.appendChild(dVerifyResult);
            }

        });

        dVerifyRow.appendChild(dVerifyButton);
        this.dVerify.appendChild(dVerifyRow);
    }

    async setup() {
        
        // Request all of the credentials from Express
        try {
        
            await this.getCredentials();
        }
        catch(err) {

            console.log(err);

        }

        // Execute the credential setup
        this.dCredentialDropdown.addEventListener('change', async (e) => {

            let value = e.target.value;

            let oData = await fSendData('./api/credentialMeta', false, { credential: value });

            this.getCredentialMeta(oData);

            // Clear results
            fClearContents(this.dPreCode);
            fClearContents(this.dQrCode);
            fClearContents(this.dVerify);
        });

    }
}

const cAPP = new App();

cAPP.setup();
