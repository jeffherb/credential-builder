// Native Modules
import PATH from 'path';
import PROCESS from 'process';
import { URL } from 'url';

// Express items
import EXPRESS from 'express';
import API_ROUTER from './src/api.js';

let sScriptPath = new URL('.', import.meta.url).pathname;

if (process.platform === "win32") {

    if (sScriptPath.indexOf(':') !== -1) {
        sScriptPath = sScriptPath.slice(sScriptPath.indexOf(':') + 1);
    }

}

(async () => {

    const iPort = 4000;

    console.log(sScriptPath);

    const APP = EXPRESS();

    // Setup static endpoints
    APP.use(EXPRESS.static(`./src/web`));

    APP.use(EXPRESS.urlencoded(
        { 
            extended: true 
        }
    ));

    APP.use(EXPRESS.json(
        // Fix to get the raw data in POSTMAN to appear
        {
            verify: function(req, res, buf, encoding) {

                req.rawBody = buf.toString();

                if (req.rawBody === "null") {
                    req.rawBody = {};
                }
            }
        }
    ));

    APP.use('/api', API_ROUTER);

    APP.get('/', (req, res) => {
        res.sendFile('./src/web/index.html');
    });

    APP.listen(iPort, () => {
        console.log(`Express has stated: http://localhost:${iPort}`);
    })

})();